package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.ComponentInputMapUIResource;

import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);

            String human = humanChoice();
            String computer = computerChoice();

            whoWon(human, computer);

            /*
             * System.out.println("Score: human" + humanScore + ", computer" +
             * computerScore);
             */

            if (readInput("Do you wish to continue playing? (y/n)?").equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter++;
        }
    }

    public String computerChoice() {
        Random rand = new Random();

        int randomNumber = rand.nextInt(0, 3);

        String choice = rpsChoices.get(randomNumber);

        /* System.out.println(rpsChoices.get(randomNumber)); */
        return choice;
    }

    public String humanChoice() {

        while (true) {
            String test = readInput("Your choice (Rock/Paper/Scissors)?");
            if (test.equals("rock") || test.equals("paper") || test.equals("scissors")) {
                return test;

            } else {
                System.out.println("I do not understand " + test + ". Could you try again?");
                continue;
            }
        }

    }

    public void whoWon(String choice1, String choice2) {

        if (choice1.equals(choice2)) {
            System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". It's a tie!");
        }
        else if (choice1.equals("rock")) {
            if (choice2.equals("paper")) {
                computerScore++;
                System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". Computer wins!");
            }
            if (choice2.equals("scissors")) {
                    humanScore++;
                    System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". Human wins!");
                }
            }
            else if (choice1.equals("paper")) {
                    if (choice2.equals("rock")) {
                        humanScore++;
                        System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". Human wins!");
                    }
                    if (choice2.equals("scissors")) {
                        computerScore++;
                        System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". Humans wins!");
                    }
                }
                else if (choice1.equals("scissors")) {
                    if (choice2.equals("rock")) {
                        computerScore++;
                        System.out
                                .println("Human chose " + choice1 + ", computer chose " + choice2 + ". Computer wins!");
                    }
                    if (choice2.equals("paper")) {
                        humanScore++;
                        System.out.println("Human chose " + choice1 + ", computer chose " + choice2 + ". Human wins!");
                    }

                }
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }

        
        

    

    // TODO: Implement Rock Paper Scissors

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
